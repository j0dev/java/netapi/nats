package nl.j0dev.netapi.nats;

import java.io.IOException;
import io.nats.client.*;

public class NatsWrapper {

    private final NatsNetApp netApp;

    private Connection connection;

    public NatsWrapper(NatsNetApp netApp) {
        this.netApp = netApp;
    }

    public boolean connect() {
        try {
            this.connection = io.nats.client.Nats.connect(this.netApp.getNatsURL());
        } catch (IOException | InterruptedException e) {
            return false;
        }
        return true;
    }

    public Connection getConnection() {
        return this.connection;
    }
    
    public boolean isConnected() {
        if (this.connection != null) {
            return this.connection.getStatus() == Connection.Status.CONNECTED;
        }
        return false;
    }

    
}
