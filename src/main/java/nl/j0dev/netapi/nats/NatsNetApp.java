package nl.j0dev.netapi.nats;

import nl.j0dev.netapi.api.NetApp;
import nl.j0dev.netapi.api.msg.MessageSubsystem;
import nl.j0dev.netapi.api.transformer.TransformerRepository;
import nl.j0dev.netapi.api.transformer.TransformerRepositoryImpl;
import nl.j0dev.netapi.nats.msg.NatsMessageSubsystem;
import nl.j0dev.netapi.nats.sd.NatsServiceDiscovery;

public class NatsNetApp implements NetApp {

    private final String name;
    private final String natsURL;

    private final NatsServiceDiscovery sd;
    private final MessageSubsystem msg;
    private final TransformerRepository transformerRepository;

    private final NatsWrapper natsWrapper;

    public NatsNetApp(String name, String natsURL) {
        // set variables
        this.name = name;
        this.natsURL = natsURL;
        // wrapper & connect
        this.natsWrapper = new NatsWrapper(this);
        this.natsWrapper.connect();
        // subsystems
        this.sd = new NatsServiceDiscovery(this);
        this.msg = new NatsMessageSubsystem(this);
        this.transformerRepository = new TransformerRepositoryImpl();
    }

    @Override
    public NatsServiceDiscovery sd() {
        return this.sd;
    }

    @Override
    public MessageSubsystem msg() {
        return this.msg;
    }

    @Override
    public TransformerRepository transformer() {
        return this.transformerRepository;
    }

    public String getName() {
        return this.name;
    }

    public String getNetID() {
        return this.name;
    }

    public String getNatsURL() {
        return this.natsURL;
    }


    public NatsWrapper wrapper() {
        return this.natsWrapper;
    }
}
