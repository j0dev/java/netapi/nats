package nl.j0dev.netapi.nats.sd;

import nl.j0dev.netapi.api.sd.NetNodeSelf;

import java.util.ArrayList;
import java.util.List;

public class NatsNodeSelf implements NetNodeSelf {

    private String name;
    private final String netID;
    private List<String> capabilities = new ArrayList<>();

    public NatsNodeSelf(String netID, String name) {
        this.name = name;
        this.netID = netID;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public List<String> getCapabilities() {
        return this.capabilities.stream().toList();
    }

    @Override
    public boolean hasCapability(String capability) {
        return this.capabilities.contains(capability);
    }

    @Override
    public String getNetID() {
        return this.netID;
    }

    @Override
    public void addCapability(String capability) {
        this.capabilities.add(capability);
    }

    @Override
    public void removeCapability(String capability) {
        this.capabilities.remove(capability);
    }

    @Override
    public void changeName(String newName) {
        this.name = newName;
    }
}
