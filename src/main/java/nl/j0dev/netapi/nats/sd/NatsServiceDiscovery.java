package nl.j0dev.netapi.nats.sd;

import nl.j0dev.netapi.api.sd.NetNode;
import nl.j0dev.netapi.api.sd.NetNodeSelf;
import nl.j0dev.netapi.api.sd.ServiceDiscovery;
import nl.j0dev.netapi.nats.NatsNetApp;

import java.util.ArrayList;
import java.util.List;

public class NatsServiceDiscovery implements ServiceDiscovery {

    private final NatsNetApp netApp;
    private final NatsNodeSelf nodeSelf;

    public NatsServiceDiscovery(NatsNetApp netApp) {
        this.netApp = netApp;
        this.nodeSelf = new NatsNodeSelf(this.netApp.getNetID(), this.netApp.getName());
    }

    @Override
    public NetNodeSelf self() {
        return nodeSelf;
    }

    @Override
    public List<NetNode> getNodes() {
        return new ArrayList<>();
    }

    @Override
    public List<NetNode> findNodes(String capability) {
        return new ArrayList<>();
    }
}
