package nl.j0dev.netapi.nats.sd;

import nl.j0dev.netapi.api.sd.NetNode;

import java.util.List;

public class NatsNetNode implements NetNode {

    private String name;
    private final String netID;
    private List<String> capabilities;

    public NatsNetNode(String netID, String name) {
        this.name = name;
        this.netID = netID;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public List<String> getCapabilities() {
        return this.capabilities.stream().toList();
    }

    @Override
    public boolean hasCapability(String capability) {
        return this.capabilities.contains(capability);
    }

    @Override
    public String getNetID() {
        return this.netID;
    }


    void overrideName(String newName) {
        this.name = newName;
    }
    void addCapability(String newCapability) {
        this.capabilities.add(newCapability);
    }
    void removeCapability(String capability) {
        this.capabilities.remove(capability);
    }
}
