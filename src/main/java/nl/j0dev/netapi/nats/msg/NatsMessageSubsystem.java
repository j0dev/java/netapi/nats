package nl.j0dev.netapi.nats.msg;

import io.nats.client.Connection;
import io.nats.client.Dispatcher;
import io.nats.client.Subscription;
import nl.j0dev.netapi.api.exceptions.ChannelMissingException;
import nl.j0dev.netapi.api.msg.*;
import nl.j0dev.netapi.api.msg.annotations.Channel;
import nl.j0dev.netapi.api.msg.handlers.RpcHandler;
import nl.j0dev.netapi.api.msg.handlers.RpcObjectHandler;
import nl.j0dev.netapi.api.transformer.MessageTopicTransformer;
import nl.j0dev.netapi.api.transformer.MessageTransformer;
import nl.j0dev.netapi.api.transformer.TransformerMissing;
import nl.j0dev.netapi.nats.NatsNetApp;
import nl.j0dev.netapi.nats.util.temp.ChannelGenerator;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

public class NatsMessageSubsystem implements MessageSubsystem {

    private final NatsNetApp netApp;

    private Connection connection;
    private Dispatcher dispatcher;

    private Map<MessageHandler, Subscription> subscriptionHandlerMap;
    private Map<MessageObjectHandler<?>, Subscription> subscriptionObjectHandlerMap;
    private Map<RpcHandler, Subscription> subscriptionRpcHandlerMap;
    private Map<RpcObjectHandler<?>, Subscription> subscriptionRpcObjectHanlerMap;

    public NatsMessageSubsystem(NatsNetApp netApp) {
        this.netApp = netApp;
    }

    /**
     * Only for internal use
     */
    public void connect() {
        this.connection = this.netApp.wrapper().getConnection();
        this.dispatcher = this.connection.createDispatcher();
        this.subscriptionHandlerMap = new HashMap<>();
        this.subscriptionObjectHandlerMap = new HashMap<>();
        this.subscriptionRpcHandlerMap = new HashMap<>();
        this.subscriptionRpcObjectHanlerMap = new HashMap<>();

        // TODO: re-setup listeners if active
    }
    /**
     * Only for internal use
     */
    public void disconnect() {
        // TODO: UnSubscribe and destroy dispatcher
    }

    @Override
    public void publish(String channel, byte[] data) {
        this.connection.publish(channel, data);
    }
    @Override
    public void publish(String channel, byte[] data, String replyChannel) {
        this.connection.publish(channel, replyChannel, data);
    }
    @Override
    public void publish(String channel, Object data) throws TransformerMissing {
        MessageTransformer<Object> transformer = (MessageTransformer<Object>) this.netApp.transformer().getMessageTransformer(data.getClass().getName());
        if (transformer == null) {
            throw new TransformerMissing(data.getClass().toString());
        }
        byte[] serialized = transformer.serialize(data);
        this.publish(channel, serialized);
    }
    @Override
    public void publish(String channel, Object data, String replyChannel) throws TransformerMissing {
        MessageTransformer<Object> transformer = (MessageTransformer<Object>) this.netApp.transformer().getMessageTransformer(data.getClass().getName());
        if (transformer == null) {
            throw new TransformerMissing(data.getClass().toString());
        }
        byte[] serialized = transformer.serialize(data);
        this.publish(channel, serialized, replyChannel);
    }
    @Override
    public void publish(Object data) throws TransformerMissing {
        MessageTransformer<Object> transformer = (MessageTransformer<Object>) this.netApp.transformer().getMessageTransformer(data.getClass().getName());
        if (transformer == null) {
            throw new TransformerMissing(data.getClass().toString());
        }
        if (transformer instanceof MessageTopicTransformer) {
            byte[] serialized = transformer.serialize(data);
            String channel = ((MessageTopicTransformer) transformer).getChannelName();
            this.publish(channel, serialized);
        } else {
            // TODO throw exception
        }
    }
    @Override
    public void publish(Object data, String replyChannel) throws TransformerMissing {
        MessageTransformer<Object> transformer = (MessageTransformer<Object>) this.netApp.transformer().getMessageTransformer(data.getClass().getName());
        if (transformer == null) {
            throw new TransformerMissing(data.getClass().toString());
        }
        if (transformer instanceof MessageTopicTransformer) {
            byte[] serialized = transformer.serialize(data);
            String channel = ((MessageTopicTransformer) transformer).getChannelName();
            this.publish(channel, serialized, replyChannel);
        } else {
            // TODO throw exception
        }
    }

    @Override
    public void subscribe(MessageHandler messageHandler) throws ChannelMissingException {
        Channel ca = messageHandler.getClass().getAnnotation(Channel.class);
        if (ca == null) throw new ChannelMissingException(messageHandler.getClass().getName());
        this.subscribe(ca.channel(), messageHandler);
    }
    @Override
    public void subscribe(String channel, MessageHandler messageHandler) {
        if (messageHandler instanceof CancelableMessageHandler) {
            ((CancelableMessageHandler) messageHandler).bindCancel(this.netApp);
        }
        io.nats.client.MessageHandler handler = message -> messageHandler.onMessage(message.getSubject(), message.getData(), message.getReplyTo());
        Subscription subscription = this.dispatcher.subscribe(channel, handler);
        this.subscriptionHandlerMap.put(messageHandler, subscription);
    }

    @Override
    public void subscribe(MessageObjectHandler<?> messageObjectHandler) throws ChannelMissingException {
        Channel ca = messageObjectHandler.getClass().getAnnotation(Channel.class);
        if (ca == null) throw new ChannelMissingException(messageObjectHandler.getClass().getName());
        this.subscribe(ca.channel(), messageObjectHandler);
    }
    @Override
    public void subscribe(String channel, MessageObjectHandler<?> messageObjectHandler) {
        if (messageObjectHandler instanceof CancelableMessageObjectHandler<?>) {
            ((CancelableMessageObjectHandler<?>) messageObjectHandler).bindCancel(this.netApp);
        }
        String klass = NatsMessageSubsystem.getObjectType(messageObjectHandler);
        MessageTransformer<Object> messageTransformer = (MessageTransformer<Object>) this.netApp.transformer().getMessageTransformer(klass);
        io.nats.client.MessageHandler handler = message -> {
            ((MessageObjectHandler<Object>) messageObjectHandler).onMessage(message.getSubject(), messageTransformer.deserialize(message.getData()), message.getReplyTo());
        };
        Subscription subscription = this.dispatcher.subscribe(channel, handler);
        this.subscriptionObjectHandlerMap.put(messageObjectHandler, subscription);
    }

    @Override
    public void subscribe(RpcHandler rpcHandler) throws ChannelMissingException {
        Channel ca = rpcHandler.getClass().getAnnotation(Channel.class);
        if (ca == null) throw new ChannelMissingException(rpcHandler.getClass().getName());
        this.subscribe(ca.channel(), rpcHandler);
    }
    @Override
    public void subscribe(String channel, RpcHandler rpcHandler) {
        io.nats.client.MessageHandler handler = message -> {
            byte[] returnMsg = rpcHandler.onMessage(message.getSubject(), message.getData(), message.getReplyTo());
            if (returnMsg != null && returnMsg.length > 0) {
                this.publish(message.getReplyTo(), returnMsg);
            }
        };
        Subscription subscription = this.dispatcher.subscribe(channel, handler);
        this.subscriptionRpcHandlerMap.put(rpcHandler, subscription);
    }

    @Override
    public void subscribe(RpcObjectHandler<?> rpcObjectHandler) throws ChannelMissingException {
        Channel ca = rpcObjectHandler.getClass().getAnnotation(Channel.class);
        if (ca == null) throw new ChannelMissingException(rpcObjectHandler.getClass().getName());
        this.subscribe(ca.channel(), rpcObjectHandler);
    }
    @Override
    public void subscribe(String channel, RpcObjectHandler<?> rpcObjectHandler) {
        String klass = NatsMessageSubsystem.getObjectType(rpcObjectHandler);
        MessageTransformer<Object> messageTransformer = (MessageTransformer<Object>) this.netApp.transformer().getMessageTransformer(klass);
        if (messageTransformer == null) {
            System.out.println("messageTransformer = " + messageTransformer);
            return;
        }
        io.nats.client.MessageHandler handler = message -> {
            Object ret = ((RpcObjectHandler<Object>) rpcObjectHandler).onMessage(message.getSubject(), messageTransformer.deserialize(message.getData()), message.getReplyTo());
            MessageTransformer<Object> returnTransformer = (MessageTransformer<Object>) this.netApp.transformer().getMessageTransformer(klass);
            this.publish(message.getReplyTo(), returnTransformer.serialize(ret));
        };
        Subscription subscription = this.dispatcher.subscribe(channel, handler);
        this.subscriptionRpcObjectHanlerMap.put(rpcObjectHandler, subscription);
    }

    @Override
    public void request(String channel, byte[] data, CancelableMessageHandler messageHandler) {
        String inbox = ChannelGenerator.generateInbox();
        this.subscribe(inbox, messageHandler);
        this.publish(channel, data, inbox);
    }
    @Override
    public void request(String channel, Object data, CancelableMessageObjectHandler<?> messageObjectHandler) throws TransformerMissing {
        String inbox = ChannelGenerator.generateInbox();
        this.subscribe(channel, messageObjectHandler);
        this.publish(channel, data, inbox);
    }

    @Override
    public void unsubscribe(Object o) {
        if (o instanceof MessageHandler && this.subscriptionHandlerMap.containsKey(o)) {
            this.dispatcher.unsubscribe(this.subscriptionHandlerMap.get(o));
            return;
        }
        if (o instanceof MessageObjectHandler && this.subscriptionObjectHandlerMap.containsKey(o)) {
            this.dispatcher.unsubscribe(this.subscriptionObjectHandlerMap.get(o));
        }
    }

    public static String getObjectType(MessageObjectHandler<?> handler) {
        // get all methods from class
        Method[] methods = handler.getClass().getDeclaredMethods();
        // iterate over methods
//        System.out.println("methods = " + methods.length);
        for (Method method : methods) {
            // skip any method not named onMessage
            if (!method.getName().equals("onMessage")) {
                continue;
            }
//            System.out.println("method name: "+ method.getName());

            // get parameters
            Class<?>[] parameterTypes = method.getParameterTypes();

            // check second parameter
            if (parameterTypes.length != 3) {
                continue;
            }

            String klass = parameterTypes[1].getName();
//            System.out.println(" param type: " + klass);
            if (klass.equals("java.lang.Object")) {
                continue;
            }
            return klass;
        }
        return null;
    }
    public static String getObjectType(RpcObjectHandler<?> handler) {
        // get all methods from class
        Method[] methods = handler.getClass().getDeclaredMethods();
        // iterate over methods
//        System.out.println("methods = " + methods.length);
        for (Method method : methods) {
            // skip any method not named onMessage
            if (!method.getName().equals("onMessage")) {
                continue;
            }
//            System.out.println("method name: "+ method.getName());

            // get parameters
            Class<?>[] parameterTypes = method.getParameterTypes();

            // check second parameter
            if (parameterTypes.length != 3) {
                continue;
            }

            String klass = parameterTypes[1].getName();
//            System.out.println(" param type: " + klass);
            if (klass.equals("java.lang.Object")) {
                continue;
            }
            return klass;
        }
        return null;
    }
}
