package nl.j0dev.netapi.nats.util.temp;

import io.nats.client.NUID;

public class ChannelGenerator {
    public static String generateInbox() {
        return "netapp._INBOX." + randomInboxSuffix();
    }

    public static String randomInboxSuffix() {
        return NUID.nextGlobal();
    }
}
